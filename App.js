import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import HomePage from './src/HomePage';
import TextBelowLogo from './src/TextBelowLogo';
import NavigatorComp from './src/NavigatorComp';
import Footer from './src/Footer';
import HelpComp from './src/HelpComp';
import FirstLoad from './src/FirstLoad';

export default function App() {
  return (
    <SafeAreaView style={{flex:1, position: 'relative'}}>
      {/* <HomePage />
      <View
        style={{ height: 2, width: '100%', backgroundColor: '#C8C8C8', opacity: 0.6 }}
      />
      <TextBelowLogo />
     
      <View
        style={{ height: 2, width: '100%', backgroundColor: '#C8C8C8', opacity: 0.6 }}
      />
      <NavigatorComp />
      <Footer/> */}
      <FirstLoad />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
