import React, { Component } from 'react'
import { Dimensions,Image, StyleSheet, Text, View,SafeAreaView,ScrollView, TouchableOpacity } from 'react-native';

const {width,height} = Dimensions.get("window")

export default class FirstLoad extends Component {
  render() {
    return (
      <View style={{flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#FF5349"}}>
        <Image style={{width:100,height:100,borderRadius:100/2}} source={require("./Assets/unnamed.png")}></Image>
      </View>
    )
  }
}
