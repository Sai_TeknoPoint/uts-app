import React, { Component } from 'react'
import { Button,StyleSheet, Text, View, Image, TouchableOpacity,Animated } from 'react-native';
import HelpComp from './HelpComp';


export default class TextBelowLogo extends Component {

    state = {
      Animated: false
    }
    
    fadeAnim = new Animated.Value(0)

      BlinkIn = () => {
        fadeIn = () => {
          // Will change fadeAnim value to 1 in 5 seconds
          Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 5000
          }).start();
        }
      
        fadeOut = () => {
          // Will change fadeAnim value to 0 in 3 seconds
          Animated.timing(this.state.fadeAnim, {
            toValue: 0,
            duration: 3000
          }).start();
        }
      }
      
      componentDidMount() {
        // setTimeout(()=>{this.setState({animationOn:!this.state.animationOn})},3000)
        setTimeout(() => {
            this.BlinkIn();
            this.setState({ Animated: true });
        }, 3000)
    }


  render() {
    return (

        <View style={{justifyContent:"center",alignItems:"center",padding:15,height:100,}}>
          <Animated.View style={[
              
              {
                // Bind opacity to animated value
                opacity: this.state.fadeAnim
              }
            ]}
          >
        <Text style={{fontSize:19,color:"green",fontWeight:"bold"}}>INDIAN RAILWAYS OFFERS 3%</Text>
        <View style={{alignItems:"center"}}>
        <Text style={{fontSize:19,color:"green",fontWeight:"bold"}}>BONUS ON RECHARGE OF</Text></View>
        <View style={{alignItems:"center"}}>
        <Text style={{fontSize:19,color:"green",fontWeight:"bold"}}>R-WALLET.</Text></View>
        </Animated.View>
        {/* <View>
          <Button title="Fade In View" onPress={this.fadeIn} />
          <Button title="Fade Out View" onPress={this.fadeOut} />
        </View> */}
        {/* <HelpComp></HelpComp> */}
      </View>
    )
  }
}


  
