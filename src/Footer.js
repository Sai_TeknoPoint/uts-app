import React, { Component } from 'react'
import { StyleSheet, Text, View,SafeAreaView,ScrollView, TouchableOpacity } from 'react-native';


export default class Footer extends Component {
  render() {
    return (
      <View style={{height:55,alignItems:"center",justifyContent:"center",paddingBottom:3}}>
        <Text style={{color:"#FF5349"}}>G.16.24 (15.0.37)</Text>
        <Text style={{color:"#FF5349"}}>Center for Railway Information Systems</Text>
        <Text style={{color:"#FF5349"}}>(CRIS)</Text>
      </View>
    )
  }
}
