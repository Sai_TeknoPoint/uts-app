import React, { Component } from 'react'
import { StyleSheet, Text, View, SafeAreaView,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'

export default class HelpComp extends Component {
    render() {
        return (
            <View style={{ zIndex: 10, position: "absolute", bottom: 60, right: 20, flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontSize: 18 }}>HELP</Text>
                <View style={{ height: 60, width: 60, backgroundColor: "#FF5349", borderRadius: 30, alignItems: "center", justifyContent: "center", marginLeft: 5,shadowColor: "#000",
    shadowOffset: {
    width: 0,
        height: 2,
},
shadowOpacity: 0.25,
    shadowRadius: 3.84,

        elevation: 5, }}>
                    <TouchableOpacity>
                    <Icon name='question-circle-o' size={25} color="white"></Icon>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


