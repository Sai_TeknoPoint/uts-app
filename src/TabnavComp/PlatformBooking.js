import React, { Component } from 'react'
import { StyleSheet, Text, View,SafeAreaView,ScrollView, TouchableOpacity,TextInput } from 'react-native';
import Icon  from 'react-native-vector-icons/Fontisto'
import SecondIcon  from 'react-native-vector-icons/FontAwesome'
import ThirdIcon  from 'react-native-vector-icons/MaterialCommunityIcons'
import ArrowIcon  from 'react-native-vector-icons/AntDesign'
import HelpComp from '../HelpComp';
import TickmarkOne from './NormalBookingsComp/TickmarkOne';
import TickmarkTwo from './NormalBookingsComp/TickmarkTwo';


export default class PlatformBooking extends Component {
  render() {
    return (
      <View style={{height:470,alignItems:"center",borderBottomWidth:2,paddingLeft:6,paddingRight:6,marginTop:10}}>
      <ScrollView>
        <View style={{ height:125,width:"100%",marginTop:15}}>
          
          <View style={{flexDirection:"row",justifyContent:"space-between",}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>CANCEL TICKET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <SecondIcon name='history' size={25}></SecondIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>BOOKING HISTORY</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket-alt' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>SHOW TICKET</Text>
            </View>
          </View>

          <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:7}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='wallet' size={25} style={{marginTop:3}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>R-WALLET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <ThirdIcon name='face-man-profile' size={25} style={{marginTop:3}}></ThirdIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>PROFILE</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='credit-card' size={20} style={{marginTop:5}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>TRANSACTIONS</Text>
            </View>
          </View>  
        </View>
      
        
        <View style={{backgroundColor:"white",height:380,width:"100%",borderRadius:15,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,}}>
              <View style={{height:50,width:"100%",backgroundColor:"#FF5349",paddingTop:13,borderTopRightRadius:15,borderTopLeftRadius:15,paddingLeft:15,}}>
                <Text style={{color:"white",fontSize:25}} >PLATFORM BOOKING</Text>
              </View> 

              <View style={{height:60,flexDirection:"row",justifyContent:"space-around"}}>
              <View style={{flexDirection:"row",alignItems:"center",padding:10}}>
                {/* <View style={[{height:20,width:20,borderWidth:3,borderRadius:30,borderColor:"grey",alignItems:"center",justifyContent:"center"},{borderColor:this.state.isSelected?"#FF5349":"grey"}]}> 
                  <TouchableOpacity onPress={this.tickmark} style={{alignItems:"center",justifyContent:"center"}}>
                  {this.state.isSelected ? (<View style={{height:10,width:10,backgroundColor:"#FF5349",borderRadius:30,}}></View>):<Text style={{color:"white"}}>?</Text>}
                  </TouchableOpacity>
                </View> */}
                
                <TickmarkOne isSelected={true}></TickmarkOne>
                <View style={{marginLeft:5}}>
                  <Text>Book & Travel</Text>
                  <Text>(Paperless)</Text>
                </View>
              </View>

              <View style={{flexDirection:"row",alignItems:"center",padding:10}}>
              {/* <View style={[{height:20,width:20,borderWidth:3,borderRadius:30,borderColor:"grey",alignItems:"center",justifyContent:"center"},{borderColor:this.state.isSelected?"#FF5349":"grey"}]}> 
                  <TouchableOpacity onPress={this.tickmark} style={{alignItems:"center",justifyContent:"center"}}>
                  {this.state.isSelected ? (<View style={{height:10,width:10,backgroundColor:"#FF5349",borderRadius:30,}}></View>):<Text style={{color:"white"}}>?</Text>}
                  </TouchableOpacity>
                </View> */}
                <TickmarkTwo isSelected={false}></TickmarkTwo>
                <View style={{marginLeft:5}}>
                  <Text>Book & Print</Text>
                  <Text>(Paper)</Text>
                </View>
              </View>



            </View>
              <View style={{height:60,borderBottomColor:"red",borderBottomWidth:2,margin:17}}>
                 <Text style={{fontSize:19,marginLeft:12}}>Station Name / Code</Text>
                 <TextInput placeholder='Station Name / Code' style={{fontSize:19,marginLeft:15}}></TextInput> 
              </View>

              <View style={{flexDirection:'row',justifyContent:"space-evenly",alignItems:"center"}}>
                <View style={{height:100,width:150,justifyContent:"center",borderBottomWidth:2,borderBottomColor:"#FF5349"}}>
                  <Text style={{fontSize:18,marginBottom:15,fontWeight:"600"}}>Person(s)</Text>
                  <Text style={{fontSize:18,marginLeft:12}}>ONE(1)</Text>
                  {/* <Text style={{fontSize:16}}>Station Name</Text> */}
                </View>
                
                <View style={{height:100,width:150,justifyContent:"center",borderBottomWidth:2,borderBottomColor:"#FF5349"}}>
                  <Text style={{fontSize:18,marginBottom:15,fontWeight:"600"}} >Payment Type</Text>
                  <Text style={{fontSize:18,marginLeft:12}}>RWALLET</Text>
                  {/* <Text style={{fontSize:16}}>Station Name</Text> */}
                </View>
               
              </View>  

            <View style={{marginTop:10,alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{marginTop:10,backgroundColor:"#FF5349",width:175,height:35,borderTopLeftRadius:15,borderBottomLeftRadius:15,borderTopRightRadius:15,borderBottomRightRadius:15,alignItems:"center",justifyContent:"center"}}
                ><Text style={{color:"white",fontSize:20}}>BOOK TICKET</Text>
                </TouchableOpacity>
              </View>
        </View>   
        <Text style={{opacity:0}}>The impact of Double Asteroid Redirection Test (DART) spacecraft's intentional collision with asteroid Dimorphos and its corresponding plumule as seen by using the Mookodi instrument on the SAAO's 1-m Lesedi telescope. NASA's first flight mission for planetary defense, DART seeks to test and validate a method to protect Earth in case of an asteroid impact threat.
            This article is about the Republic of India. For other uses, see India (disambiguation).
Republic of India
Bhārat Gaṇarājya
(see other local names)
Horizontal tricolour flag bearing, from top to bottom, deep saffron, white, and green horizontal bands. In the centre of the white band is a navy-blue wheel with 24 spokes.
Flag
Three lions facing left, right, and toward viewer, atop a frieze containing a galloping horse, a 24-spoke wheel, and an elephant. Underneath is a motto: "सत्यमेव जयते".
State emblem
Motto: "Satyameva Jayate" (Sanskrit)
"Truth Alone Triumphs"[1]
Anthem: "Jana Gana Mana"[2][3]
"Thou Art the Ruler of the Minds of All People"[4][2]
1:04
National song
"Vande Mataram" (Sanskrit)
"I Bow to Thee, Mother"[a][1][2]
Image of a globe centred on India, with India highlighted.
Territory controlled by India shown in dark green; territory claimed but not controlled shown in light green
Capital	New Delhi
28°36′50″N 77°12′30″E
Largest city	
Mumbai (city proper)
Delhi (metropolitan area)
Official languages	
HindiEnglish[b][8]
Recognised national languages	None[9][10][11]
Recognised regional languages	
State level and Eighth Schedule[12]
Native languages	447 languages[c]
Religion (2011)	
79.8% Hinduism
14.2% Islam
2.3% Christianity
1.7% Sikhism
0.7% Buddhism
0.4% Jainism
0.23% Unaffiliated
0.65% Others[15]
Demonym(s)	Indian
Government	Federal parliamentary constitutional republic
• President
Droupadi Murmu
• Vice President and Chairman of Rajya Sabha
Jagdeep Dhankhar
• Prime Minister
Narendra Modi
• Chief Justice
Dhananjaya Y. Chandrachud
• Lok Sabha Speaker
Om Birla
Legislature	Parliament
• Upper house
Rajya Sabha
• Lower house
Lok Sabha
Independence from the United Kingdom
• Dominion
15 August 1947
• Republic
26 January 1950
Area
• Total
3,287,263[2] km2 (1,269,219 sq mi)[d] (7th)
• Water (%)
9.6
Population
• 2022 estimate
1,375,586,000[17] (2nd)
• 2011 census
1,210,854,977[18][19] (2nd)
• Density
418.0/km2 (1,082.6/sq mi) (19th)
GDP (PPP)	2022 estimate
• Total
Increase $11.665 trillion[20] (3rd)
• Per capita
Increase $8,293[20] (127th)
GDP (nominal)	2022 estimate
• Total
Increase $3.469 trillion[20] (5th)
• Per capita
Increase $2,466[20] (139th)
Gini (2011)	35.7[21][22]
medium · 98th
HDI (2021)	Decrease 0.633[23]
medium · 132nd
Currency	Indian rupee (₹) (INR)
Time zone	UTC+05:30 (IST)
DST is not observed
Date format	
dd-mm-yyyy[e]
Driving side	left[24]
Calling code	+91
ISO 3166 code	IN
Internet TLD	.in (others)
India, officially the Republic of India (Hindi: Bhārat Gaṇarājya),[25] is a country in South Asia. It is the seventh-largest country by area, the second-most populous country, and the most populous democracy in the world. Bounded by the Indian Ocean on the south, the Arabian Sea on the southwest, and the Bay of Bengal on the southeast, it shares land borders with Pakistan to the west;[f] China, Nepal, and Bhutan to the north; and Bangladesh and Myanmar to the east. In the Indian Ocean, India is in the vicinity of Sri Lanka and the Maldives; its Andaman and Nicobar Islands share a maritime border with Thailand, Myanmar, and Indonesia. The nation's capital city is New Delhi.

Modern humans arrived on the Indian subcontinent from Africa no later than 55,000 years ago.[26][27][28] Their long occupation, initially in varying forms of isolation as hunter-gatherers, has made the region highly diverse, second only to Africa in human genetic diversity.[29] Settled life emerged on the subcontinent in the western margins of the Indus river basin 9,000 years ago, evolving gradually into the Indus Valley Civilisation of the third millennium BCE.[30] By 1200 BCE, an archaic form of Sanskrit, an Indo-European language, had diffused into India from the northwest.[31][32] Its evidence today is found in the hymns of the Rigveda. Preserved by a resolutely vigilant oral tradition, the Rigveda records the dawning of Hinduism in India.[33] The Dravidian languages of India were supplanted in the northern and western regions.[34] By 400 BCE, stratification and exclusion by caste had emerged within Hinduism,[35] and Buddhism and Jainism had arisen, proclaiming social orders unlinked to heredity.[36] Early political consolidations gave rise to the loose-knit Maurya and Gupta Empires based in the Ganges Basin.[37] Their collective era was suffused with wide-ranging creativity,[38] but also marked by the declining status of women,[39] and the incorporation of untouchability into an organised system of belief.[g][40] In South India, the Middle kingdoms exported Dravidian-languages scripts and religious cultures to the kingdoms of Southeast Asia.[41]

In the early medieval era, Christianity, Islam, Judaism, and Zoroastrianism became established on India's southern and western coasts.[42] Muslim armies from Central Asia intermittently overran India's northern plains,[43] eventually founding the Delhi Sultanate, and drawing northern India into the cosmopolitan networks of medieval Islam.[44] In the 15th century, the Vijayanagara Empire created a long-lasting composite Hindu culture in south India.[45] In the Punjab, Sikhism emerged, rejecting institutionalised religion.[46] The Mughal Empire, in 1526, ushered in two centuries of relative peace,[47] leaving a legacy of luminous architecture.[h][48] Gradually expanding rule of the British East India Company followed, turning India into a colonial economy, but also consolidating its sovereignty.[49] British Crown rule began in 1858. The rights promised to Indians were granted slowly,[50][51] but technological changes were introduced, and modern ideas of education and the public life took root.[52] A pioneering and influential nationalist movement emerged, which was noted for nonviolent resistance and became the major factor in ending British rule.[53][54] In 1947 the British Indian Empire was partitioned into two independent dominions,[55][56][57][58] a Hindu-majority Dominion of India and a















              
            </Text>   
        
        
        </ScrollView>
  <HelpComp></HelpComp>
        
    </View>
        )
  }
}
