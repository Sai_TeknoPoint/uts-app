import React, { Component } from 'react'
import { StyleSheet, Text, View,SafeAreaView,ScrollView, TouchableOpacity,TextInput } from 'react-native';
import Icon  from 'react-native-vector-icons/Fontisto'
import SecondIcon  from 'react-native-vector-icons/FontAwesome'
import ThirdIcon  from 'react-native-vector-icons/MaterialCommunityIcons'
import ArrowIcon  from 'react-native-vector-icons/AntDesign'
import HelpComp from '../HelpComp';
import TickmarkOne from './NormalBookingsComp/TickmarkOne';
import TickmarkTwo from './NormalBookingsComp/TickmarkTwo';


export default class QRBooking extends Component {
  render() {
    return (
      <View style={{height:470,alignItems:"center",borderBottomWidth:2,paddingLeft:6,paddingRight:6,marginTop:10}}>
      <ScrollView>
        <View style={{ height:125,width:"100%",marginTop:15}}>
          
          <View style={{flexDirection:"row",justifyContent:"space-between",}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>CANCEL TICKET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <SecondIcon name='history' size={25}></SecondIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>BOOKING HISTORY</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket-alt' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>SHOW TICKET</Text>
            </View>
          </View>

          <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:7}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='wallet' size={25} style={{marginTop:3}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>R-WALLET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <ThirdIcon name='face-man-profile' size={25} style={{marginTop:3}}></ThirdIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>PROFILE</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='credit-card' size={20} style={{marginTop:5}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>TRANSACTIONS</Text>
            </View>
          </View>  
        </View>
      
        
        <View style={{backgroundColor:"white",height:180,width:"100%",borderRadius:15,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,}}>
              <View style={{height:50,width:"100%",backgroundColor:"#FF5349",paddingTop:13,borderTopRightRadius:15,borderTopLeftRadius:15,paddingLeft:15,}}>
                <Text style={{color:"white",fontSize:25}} >QR BOOKING</Text>
              </View> 

             

            <View style={{flexDirection:"column",alignItems:"center",justifyContent:"center",height:130}}>
              <View style={{alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity style={{marginTop:10,backgroundColor:"#FF5349",width:145,height:35,borderTopLeftRadius:15,borderBottomLeftRadius:15,borderTopRightRadius:15,borderBottomRightRadius:15,alignItems:"center",justifyContent:"center"}}
                  ><Text style={{color:"white",fontSize:16}}>JOURNEY BY QR</Text>
                  </TouchableOpacity>
              </View>
             
              <View style={{marginTop:10,alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity style={{marginTop:10,backgroundColor:"#FF5349",width:145,height:35,borderTopLeftRadius:15,borderBottomLeftRadius:15,borderTopRightRadius:15,borderBottomRightRadius:15,alignItems:"center",justifyContent:"center"}}
                  ><Text style={{color:"white",fontSize:16}}>PLATFORM BY QR</Text>
                  </TouchableOpacity>
              </View>
            </View>
        </View>      
        
        
        </ScrollView>
  <HelpComp></HelpComp>
        
    </View>
        )
  }
}
