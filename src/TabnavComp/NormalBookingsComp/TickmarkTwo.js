import React, { Component } from 'react'
import { Button,StyleSheet, Text, View, Image, TouchableOpacity,Animated } from 'react-native';


export default class TickmarkTwo extends Component {
    state = {
        isSelected : this.props.isSelected,
      }
    
      tickmark =() => {
        console.log("isSelected:",!this.state.isSelected);
          this.setState({isSelected:!this.state.isSelected});
      }
  render() {
    return (
            <View style={[{height:20,width:20,borderWidth:3,borderRadius:30,borderColor:"grey",alignItems:"center",justifyContent:"center"},{borderColor:this.state.isSelected?"#FF5349":"grey"}]}> 
                    <TouchableOpacity onPress={this.tickmark} style={{alignItems:"center",justifyContent:"center"}}>
                    {/* {this.state.isSelected ? (<Text>✓</Text>):<Text>?</Text>} */}
                    {this.state.isSelected ? (<View style={{height:10,width:10,backgroundColor:"#FF5349",borderRadius:30,}}></View>):<Text style={{color:"white"}}>?</Text>}
                    </TouchableOpacity>
            </View>
        // <View style={{flexDirection:"column"}}>
            // {/* <View style={{height:100,backgroundColor:"green"}}>
            //     <Text>* Use this option if you are outside station</Text>
            //     <Text>premises/Railway track.</Text>
            //     <Text>* Use show ticket option on mobile is the</Text>
            //     <Text>travel authority.</Text>
            //     <Text>* Set your phone GPS to high accuracy mode.</Text>
            // </View> */}
        // </View>
    )
  }
}
