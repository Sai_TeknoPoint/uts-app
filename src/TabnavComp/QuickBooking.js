import React, { Component } from 'react'
import { StyleSheet, Text, View,SafeAreaView,ScrollView, TouchableOpacity } from 'react-native';
import Icon  from 'react-native-vector-icons/Fontisto'
import SecondIcon  from 'react-native-vector-icons/FontAwesome'
import ThirdIcon  from 'react-native-vector-icons/MaterialCommunityIcons'
import ArrowIcon  from 'react-native-vector-icons/AntDesign'
import HelpComp from '../HelpComp';
import TickmarkOne from './NormalBookingsComp/TickmarkOne';
import TickmarkTwo from './NormalBookingsComp/TickmarkTwo';

export default class QuickBooking extends Component {

  

  render() {

    return (
      
      <View style={{height:470,alignItems:"center",borderBottomWidth:2,paddingLeft:6,paddingRight:6,marginTop:10}}>
      <ScrollView>
        <View style={{ height:125,width:"100%",marginTop:15}}>
          
          <View style={{flexDirection:"row",justifyContent:"space-between",}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>CANCEL TICKET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <SecondIcon name='history' size={25}></SecondIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>BOOKING HISTORY</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='ticket-alt' size={25}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>SHOW TICKET</Text>
            </View>
          </View>

          <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:7}}>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='wallet' size={25} style={{marginTop:3}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>R-WALLET</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <ThirdIcon name='face-man-profile' size={25} style={{marginTop:3}}></ThirdIcon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>PROFILE</Text>
            </View>
            <View style={{backgroundColor:"white",alignItems:"center",height:50,width:"32%",shadowColor: "#000",
shadowOffset: {
width: 0,
height: 1,
},
shadowOpacity: 0.22,
shadowRadius: 2.22,

elevation: 3,}}>
              <Icon name='credit-card' size={20} style={{marginTop:5}}></Icon>
              <Text style={{color:"#FF5349",fontWeight:"bold",marginTop:4,fontSize:12}}>TRANSACTIONS</Text>
            </View>
          </View>  
        </View>
      
        
        <View style={{backgroundColor:"white",height:180,width:"100%",borderRadius:15,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,}}>
              <View style={{height:50,width:"100%",backgroundColor:"#FF5349",paddingTop:13,borderTopRightRadius:15,borderTopLeftRadius:15,paddingLeft:15,}}>
                <Text style={{color:"white",fontSize:25}} >QUICK BOOKING</Text>
              </View> 

              <View style={{height:60,flexDirection:"row",justifyContent:"space-around"}}>
              <View style={{flexDirection:"row",alignItems:"center",padding:10}}>
                {/* <View style={[{height:20,width:20,borderWidth:3,borderRadius:30,borderColor:"grey",alignItems:"center",justifyContent:"center"},{borderColor:this.state.isSelected?"#FF5349":"grey"}]}> 
                  <TouchableOpacity onPress={this.tickmark} style={{alignItems:"center",justifyContent:"center"}}>
                  {this.state.isSelected ? (<View style={{height:10,width:10,backgroundColor:"#FF5349",borderRadius:30,}}></View>):<Text style={{color:"white"}}>?</Text>}
                  </TouchableOpacity>
                </View> */}
                
                <TickmarkOne isSelected={true}></TickmarkOne>
                <View style={{marginLeft:5}}>
                  <Text>Book & Travel</Text>
                  <Text>(Paperless)</Text>
                </View>
              </View>

              <View style={{flexDirection:"row",alignItems:"center",padding:10}}>
              {/* <View style={[{height:20,width:20,borderWidth:3,borderRadius:30,borderColor:"grey",alignItems:"center",justifyContent:"center"},{borderColor:this.state.isSelected?"#FF5349":"grey"}]}> 
                  <TouchableOpacity onPress={this.tickmark} style={{alignItems:"center",justifyContent:"center"}}>
                  {this.state.isSelected ? (<View style={{height:10,width:10,backgroundColor:"#FF5349",borderRadius:30,}}></View>):<Text style={{color:"white"}}>?</Text>}
                  </TouchableOpacity>
                </View> */}
                <TickmarkTwo isSelected={false}></TickmarkTwo>
                <View style={{marginLeft:5}}>
                  <Text>Book & Print</Text>
                  <Text>(Paper)</Text>
                </View>
              </View>


            </View>
            <View style={{marginTop:10,alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{marginTop:10,backgroundColor:"#FF5349",width:175,height:35,borderTopLeftRadius:15,borderBottomLeftRadius:15,borderTopRightRadius:15,borderBottomRightRadius:15,alignItems:"center",justifyContent:"center"}}
                ><Text style={{color:"white",fontSize:20}}>Next</Text>
                </TouchableOpacity>
              </View>
        </View>      
        
        
        </ScrollView>
  <HelpComp></HelpComp>
        
    </View>
        )
  }
}

