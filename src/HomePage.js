import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity,Modal,Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SecondIcon from 'react-native-vector-icons/Entypo';

const {height,width} = Dimensions.get('window');


export default class HomePage extends Component {

  state = {
    showModal : false
  }

  render() {
    return (
        <View style={{ backgroundColor: "#FF5349", height: 75, width: "100%", flexDirection: "row",justifyContent:"space-evenly",alignItems:"center" }}>
          <Image source={require("./Assets/unnamed.png")} style={{ height: 45, width: 45, zIndex: 1, borderRadius: 50 }}></Image>
          <View>
            <Text style={{ color: "#fff", fontSize: 29, fontWeight: 'bold' }}>UTS</Text>
            <Text style={{ color: "#fff",fontSize: 15  }}>IR Unreserved Ticketing</Text>
          </View>
          {/* <View style={{flexDirection:'row', justifyContent:"space-between",backgroundColor:"black"}}> */}
            <Icon name='google-translate' size={25} color="#fff"></Icon>
            <TouchableOpacity style={{ backgroundColor: "#FF0000", height: 30,width:30,borderRadius:45,justifyContent:"center" }} ><Text style={{fontSize:10,color:"#fff",fontWeight:"bold"}}>LOGIN</Text></TouchableOpacity>
            <View>
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.state.showModal}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          {/*All views of Modal*/}
          {/*Animation can be slide, slide, none*/}
          <View style={styles.modal}>
            <View>
            {/* <Text style={styles.text}>Modal is open!</Text> */}
            <Text style={styles.text}>UserID: nCvRaMyZQI</Text>
            <Text style={styles.text}>Registration</Text>
            <Text style={styles.text}>Notification</Text>
            {/* <Button
              title="Click To Close Modal"
              onPress={() => {
                setShowModal(!showModal);
              }}
            /> */}</View>
            <View>
            <SecondIcon name='dots-three-vertical' size={25} onPress={() => {
                // setShowModal(!showModal);
                this.setState({showModal:!this.state.showModal})
              }}  />
              </View>  
          </View>
        </Modal>
        {/*Button will change state to true and view will re-render*/}
        {/* <Button
          title="Click To Open Modal"
          onPress={() => {
            setShowModal(!showModal);
          }}
        /> */}
        <SecondIcon name='dots-three-vertical' size={25} onPress={() => {
                this.setState({showModal:!this.state.showModal})
              }}  />
      </View>
                  
          {/* </View> */}
        </View>

       
    )
  }
}


const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    // justifyContent: "flex-end",
    backgroundColor: 'red',
    marginTop: 30,
  },
  modal: {
    // alignItems: 'center',
    justifyContent:"space-around",
    flexDirection:"row",
    backgroundColor: 'white',
    height: 100,
    width:"65%",
    marginLeft:142,
    padding:5
  },
  text: {
    color: '#3f2949',
    marginTop: 10,
    fontSize:16
  },
});