
import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
// import Icon from 'react-native-vector-icons/AntDesign'
import NormalBooking from './TabnavComp/NormalBooking';
import QuickBooking from './TabnavComp/QuickBooking';
import PlatformBooking from './TabnavComp/PlatformBooking';
import SeasonBooking from './TabnavComp/SeasonBooking';
import QRBooking from './TabnavComp/QRBooking';
import Icon  from 'react-native-vector-icons/Entypo'
import SecondIcon  from 'react-native-vector-icons/MaterialCommunityIcons'
import HelpComp from './HelpComp';


function HomeScreen() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
      <NormalBooking></NormalBooking>
    </View>
  );
}

function SettingsScreen() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <QuickBooking></QuickBooking>
    </View>
  );
}

function Platform() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
     <PlatformBooking></PlatformBooking>
    </View>
  );
}

function Season() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <SeasonBooking/>
    </View>
  );
}

function QR() {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <QRBooking/>
    </View>
  );
}

const Tab = createMaterialTopTabNavigator();

export default function App() {
  return (
    <NavigationContainer >
      <Tab.Navigator>
        <Tab.Screen name="Home"
         component={HomeScreen}
         options={{
          tabBarLabel: 'Normal Booking',
          tabBarIcon: ({ color }) => (
            // <MaterialCommunityIcons name="home" color={color} size={26} />
            <Icon name='ticket' size={25} color={color} ></Icon>
          ),
          tabBarStyle: { height:85 },
          tabBarLabelStyle: { fontSize: 12,width:80,marginTop:5 },
        }}
         />
        {/* <Tab.Screen name="Settings" component={SettingsScreen} /> */}

        <Tab.Screen name="Quick Booking"
         component={SettingsScreen}
         options={{
          tabBarLabel: 'Quick   Booking',
          tabBarIcon: () => (
            // <MaterialCommunityIcons name="home" color={color} size={26} />
            // <Icon name='ticket' size={20} color={color} ></Icon>
            <SecondIcon name='cash-fast' size={25} color="black"  />
          ),
          tabBarStyle: { height:85 },
          tabBarLabelStyle: { fontSize: 12,width:80,marginTop:5 }, 
        }}
         />

      <Tab.Screen name="Platform Booking"
         component={Platform}
         options={{
          tabBarLabel: 'Platform Booking',
          tabBarIcon: () => (
            // <MaterialCommunityIcons name="home" color={color} size={26} />
            // <Icon name='ticket' size={20} color={color} ></Icon>
            <SecondIcon name='train' size={25} color="black"  />
          ),
          tabBarStyle: { height:85 },
          tabBarLabelStyle: { fontSize: 12,width:80,marginTop:5 },
        }}
         />

<Tab.Screen name="Season Booking"
         component={Season}
         options={{
          tabBarLabel: 'Season Booking',
          tabBarIcon: () => (
            // <MaterialCommunityIcons name="home" color={color} size={26} />
            // <Icon name='ticket' size={20} color={color} ></Icon>
            <SecondIcon name='ticket' size={25} color="black"  />
          ),
          tabBarStyle: { height:85 },
          tabBarLabelStyle: { fontSize: 12,width:80,marginTop:5 },
        }}
         />

<Tab.Screen name="QR Booking"
         component={QR}
         options={{
          tabBarLabel: 'QR          Booking',
          tabBarIcon: () => (
            // <MaterialCommunityIcons name="home" color={color} size={26} />
            // <Icon name='ticket' size={20} color={color} ></Icon>
            <SecondIcon name='barcode-scan' size={25} color="black"  />
          ),
          tabBarStyle: { height:85 },
          tabBarLabelStyle: { fontSize: 12,width:80,marginTop:5 },
        }}
         />
      </Tab.Navigator>
    </NavigationContainer>
  );
}








